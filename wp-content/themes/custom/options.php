<?php

//see http://wptheming.com/options-framework-plugin/ & https://github.com/devinsays/options-framework-plugin/blob/master/options-check/options.php

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 * To retrieve an option in your theme use of_get_option($id,$default)
 */

function optionsframework_options() {

    $options = array();

    $options[] = array(
        'name' => 'Basic Settings',
        'type' => 'heading');

    $options[] = array(
        'name' => 'Debug',
        'desc' => 'Enable debug mode for developing.',
        'id' => 'debug',
        'std' => '1',
        'type' => 'checkbox');

    //logo
    $options[] = array(
        'name' => "Logo",
        'desc' => "",
        'id' => 'logo',
        'type' => 'upload');

    //footer
    $wp_editor_settings = array(
        'wpautop' => true, // Default
        'textarea_rows' => 10,
        'media_buttons' => true,
        'tinymce' => array( 'plugins' => 'wordpress' )
    );

    $options[] = array(
        'name' => "Footer",
        'desc' => '',
        'id' => 'footer',
        'type' => 'editor',
        'settings' => $wp_editor_settings );

    return $options;
}

/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 *
 */
function optionsframework_option_name(){
    // This gets the theme name from the stylesheet (lowercase and without spaces)
    $themename = get_option( 'stylesheet' );
    $themename = preg_replace("/\W/", "_", strtolower($themename) );

    $optionsframework_settings = get_option('optionsframework');
    $optionsframework_settings['id'] = $themename;
    update_option('optionsframework', $optionsframework_settings);
}