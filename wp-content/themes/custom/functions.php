<?php

//////////////////////////////////////// load scripts and styles

function loadScriptsAndStyles() {
    wp_register_style( 'style', get_template_directory_uri() . '/css/style.css' );
    wp_enqueue_style("style");

    //wp_enqueue_script( 'jquery2', get_template_directory_uri() . '/js/jquery-2.0.3.min.js');
    wp_register_script( 'jquery1.10', get_template_directory_uri() . '/js/jquery-1.10.2.min.js');
    wp_register_script( 'jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate-1.2.1.min.js');
    wp_register_script( 'userscript', get_template_directory_uri() . '/js/script.js');
    wp_enqueue_script("jquery1.10");
    wp_enqueue_script("jquery-migrate");
    wp_enqueue_script("userscript");
}

add_action( 'wp_enqueue_scripts', 'loadScriptsAndStyles' );


//////////////////////////////////////// custom image size
//add_image_size( 'custom-size', 600, 150, true );


//////////////////////////////////////// custom post types

//include "customPostTypes.php";


//////////////////////////////////////// theme menus

function registerMenus() {
    register_nav_menu('menu',__( 'Menu' ));
}
add_action( 'init', 'registerMenus' );


//////////////////////////////////////// theme widgets

/*function my_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Sidebar', 'my widget' ),
        'id'            => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
        ) );
}
add_action( 'widgets_init', 'my_widgets_init' );*/


//////////////////////////////////////// custom dashbord widget
include "dashbordWidget.php";


//////////////////////////////////////// wordpress tweaks

//-------------------- tweak wordpress admin menu

/*function remove_menu_settings() {
    // the parameter is the slug of the menu item to be removed

    // hide default wordpress post
    remove_menu_page('index.php'); 
    remove_menu_page('edit.php');

    // hide pages if not admin
    if ( !current_user_can('switch_themes') ) {
        remove_menu_page('edit.php?post_type=page');
    }
}
add_action( 'admin_menu', 'remove_menu_settings' );*/


//-------------------- tweak wordpress admin quicklink options

/*function remove_quicklinks(){
    global $wp_admin_bar;
    // hide default wordpress post
    $wp_admin_bar->remove_menu('new-post');
}
add_action( 'wp_before_admin_bar_render', 'remove_quicklinks' );*/


//-------------------- hide default wordpress text field from page

/*function remove_editor() {
    remove_post_type_support('page', 'editor');
}
add_action('admin_init', 'remove_editor');*/


//-------------------- remove comments
include "removeComments.php";


//--------------------  remove unnecessary stuff in head
function headCleanup() {
    // EditURI link
    remove_action( 'wp_head', 'rsd_link' );
    // windows live writer
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // index link
    remove_action( 'wp_head', 'index_rel_link' );
    // previous link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    // start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    // links for adjacent posts
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    // WP version
    remove_action( 'wp_head', 'wp_generator' );
}
add_action( 'init', 'headCleanup' );


//-------------------- debug option
/**
 * global isDebug utility
 */
function isDebug(){
    return of_get_option("debug", false);
}


/**
 * Helper function to return the theme option value. If no value has been saved, it returns $default.
 * Needed because options are saved as serialized strings.
 *
 * This code allows the theme to work without errors if the Options Framework plugin has been disabled.
 */
if ( !function_exists( 'of_get_option' ) ) {
    function of_get_option($name, $default = false) {
        $optionsframework_settings = get_option('optionsframework');
        // Gets the unique option id
        $option_name = $optionsframework_settings['id'];
        if ( get_option($option_name) ) {
            $options = get_option($option_name);
        }
        if ( isset($options[$name]) ) {
            return $options[$name];
        } else {
            return $default;
        }
    }
}
