$(function(){
        /** RESIZER **/

        // some variables
        // grab the .wrapper container div and add in the handle.
        var snaps = [320,480,768,1024,1280, 1600];
        var container = $('.wrapper').css({position:'relative'});
        var header = $(".header");
        var wrapper = $(".wrapper");
        container.prepend('<a href="#" class="handle"></a>');
        var cover = $('.cover');                        // grab the cover (for preventing stray mouse events)
        var winw = $(window).width();                   // width of the full window.
        var z_idx, drg_h, drg_w, pos_y, pos_x;          // a few vars for helping drag
        var handle = $('.handle');                      // the handle (added above).
        var currentWidth = 240 + Math.floor(Math.random()*784); // default starting width (random).
        var lowerLimit = 240;                           // smallest width of viewport.

        //remove the debugger button in the frame
        $("#iframe").load(function(){
            $(this).contents().find(".debugButton").hide();
            //overwrite the debug mode for grid overlay
            $("#iframe").contents().find("head").append('<style>.debug-grid-item:before{content: "" !important;background-image: none !important;margin: 0 !important;padding: 0 !important;display: none !important;}</style>');

            //read cookie
            if(getCookie("showOutlines") == "1")
                toggleOutlines();

            if(getCookie("showGrid") == "1")
                toggleGrid();
        });

        //draw snaps
        function renderSnaps()
        {
            $('.markers').empty();
            for (var i = snaps.length - 1; i >= 0; i--) {
                var num = parseInt(snaps[i]);
                var mark = $('<div class="mark">'+num+'</div>').css({left:(num/2)});
                $('.markers').append(mark);
            };
        }
        renderSnaps();

        //draw size selectors
        var snaps = [320,480,768,1024,1280, 1600];
        function renderSizes()
        {
            $('.sizes').empty();

            for (var i = 0; i < snaps.length; i++){
                var num = parseInt(snaps[i]);
                var mark = $('<a class="sizeSelect" id="sizeSelect'+num+'">'+num+'</a>');
                mark.data("size", num);
                mark.click(function(){
                    setContentWidthAnimated($(this).data("size"));
                });
                $('.sizes').append(mark);
            };

            var mark = $('<a class="sizeSelect">random</a>');
            mark.click(function(){
                setContentWidthRandom();
            });
            $('.sizes').append(mark);

            mark = $('<a class="sizeSelect">full</a>');
            mark.click(function(){
                setContentWidthFull();
            });
            $('.sizes').append(mark);

        }
        renderSizes();

        function resize(){
            wrapper.height($(window).height() - header.height());
            wrapper.css("margin-top", header.height());
            console.log(wrapper.height());
            $("body").height($(document).height());
        }
        $(window).resize(function() {
            resize();
        });
        resize();


        function setContentWidth(w){
            if(w == undefined)
                w = container.width();
            else
                container.width(w);
            w = Math.round(w);

            $(".sizeSelect").removeClass("active");
            if(snaps.indexOf(w) != -1){
                $("#sizeSelect"+w).addClass("active");
            }

            var ems = Math.round((w/16),0);
            handle.text(w+ ':'+ems+'em');
            handle.css({top:Math.round(handle.css("top"))+'px',left:w+'px'});
            setCookie("content-size", w);
        }
        function setContentWidthAnimated(w){
            handle.fadeOut();
            container.animate({width: w}, {duration: 500, progress: function(){setContentWidth(undefined)}, complete: function(){setContentWidth(w); handle.fadeIn();}});
        }
        function setContentWidthRandom(){
            setContentWidthAnimated(320 + Math.random() * ($(document).width() - 320));
        }
        function setContentWidthFull(){
            setContentWidthAnimated($(document).width());
        }
        var contentSizeBegin = getCookie("content-size");
        if(contentSizeBegin == null){
            setContentWidth($(document).width());
        }else{
            setContentWidth(contentSizeBegin);
        }

        /** GRID **/
        $("#toggleGrid").click(function(){
            toggleGrid();
        })

        $("#toggleOutlines").click(function(){
            toggleOutlines();
        })

        function toggleGrid(){
            var body =  $("#iframe").contents().find("body");
            if($("#debug-grid-data").length == 0){
                //get the number of grid columns from scss
                var html = "<div id='debug-grid-data'></div>"
                $('body').prepend(html);
                var numColumns = parseInt($("#debug-grid-data").css("content").replace("'",""));
                console.log(numColumns);

                //add grid to iframe
                html = "<div id='debug-grid'></div>";
                body.prepend(html);
                var debugGrid = body.find("#debug-grid");
                debugGrid.css({
                    opacity:.3,
                    width: '100%',
                    height: '100%',
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    pointerEvents:'none'
                });
                for(var i=0;i<numColumns;i++){
                    html = "<div class='debug-grid-item grid-1' id='debug-grid-item-"+i+"'><div class='inside'>&nbsp;</div></div>";
                    debugGrid.append(html);
                    var gridItem = debugGrid.find("#debug-grid-item-"+i);
                    gridItem.css({
                        height: '100%'
                    });
                    gridItem.find(".inside").css({
                        height: '100%',
                        backgroundColor: 'rgba(0, 0, 0, .2)',
                        borderLeft: '1px solid blue',
                        borderRight: '1px solid blue',
                    });
                }

                $("#toggleGrid").addClass("active");
                setCookie("showGrid", "1");
            }else{
                $("#debug-grid-data").remove();
                body.find("#debug-grid").remove();
                $("#toggleGrid").removeClass("active");
                setCookie("showGrid", "0");
            }
        }


        function toggleOutlines(){
            var body =  $("#iframe").contents().find("body");
            console.log(body);
            if(body.hasClass("gridle-debug")){
                body.removeClass("gridle-debug");
                $("#toggleOutlines").removeClass("active");
                setCookie("showOutlines", "0");
            }else{
                body.addClass("gridle-debug");
                $("#toggleOutlines:before").hide();
                $("#toggleOutlines").addClass("active");
                setCookie("showOutlines", "1");
            }
        }

        function toggleGridOverlay(){
            if(!$('body').hasClass("gridle-debug")) {
                var height = $('body').height();
                ///var html = '<article id="grid"><div class="overlay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></article>';
                var html = "<div id='debug-grid'></div>"
                $('body').prepend(html);
                var numColumns = parseInt($("#debug-grid").css("content").replace("'",""));


                $('body').addClass("gridle-debug");
                gridOnResize();
            }else{
                $('body').removeClass("gridle-debug");
            }
        }

        $('.gridButton').click(function(){
        });

        $(window).resize(function() {
            //gridOnResize();
        });

        function gridOnResize(){
            if($('#grid').length > 0) {
                var height = $('body').height();
                if(height < $(window).innerHeight()){
                    height = $(window).innerHeight();
                    if($('#wpadminbar').length > 0)
                        height = height - $('#wpadminbar').height();
                }

                $('#grid div').css('height', height);
            }
        }


        /** DRAG **/

        handle.on('click',function(e){e.preventDefault();}); // ignore normal clicks.

        handle.on('mousedown',function(e)
        {
            // cover the whole thing coz we're only after mousemove and mouseup events now!
            cover.show();

            // add class and bring to front (for dragging).
            handle.addClass('draggable').css('z-index', 1000);

            // grab and set the pertinent values.
            z_idx = handle.css('z-index'),
                drg_h = handle.outerHeight(),
                drg_w = handle.outerWidth(),
                pos_y = handle.offset().top + drg_h - e.pageY,
                pos_x = handle.offset().left + drg_w - e.pageX;

        });

        $(window).on('dragstart',function(e){
            e.preventDefault();
        });

        $(window).on("mousemove", function(e)
        {
            if (handle.hasClass('draggable'))
            {
                var t = e.pageY + pos_y - drg_h;
                var l = e.pageX + pos_x - drg_w;

                var answer = 2 * (l - winw/2);

                if (answer < lowerLimit)
                {
                    answer = lowerLimit;
                    l = winw/2 + lowerLimit/2;
                } else {

                    var snap = 20;
                    for (var i = snaps.length - 1; i >= 0; i--) {
                        var n = snaps[i];
                        if (Math.abs(answer-n) < snap)
                        {
                            answer = n;
                            l = 1 + winw/2 + n/2;
                        }
                    };
                }

                setContentWidth(answer);

                handle.css({top:Math.round(t - header.height())+'px',left:answer+'px'});
            }

        }).on("mouseup", function() // mouseup end it all...
            {
                // console.log('up');
                cover.hide();
                handle.removeClass('draggable').css('z-index', z_idx);
                var num = container.width();
                // document.location.href = '#'+num;
                history.pushState({number:num}, '', '#'+num);
            });


        /** COOKIE **/
        function setCookie(c_name,value)
        {
            var exdays = 5;
            var exdate=new Date();
            exdate.setDate(exdate.getDate() + exdays);
            var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
            document.cookie=c_name + "=" + c_value;
        }

        function getCookie(c_name)
        {
            var c_value = document.cookie;
            var c_start = c_value.indexOf(" " + c_name + "=");
            if (c_start == -1)
            {
                c_start = c_value.indexOf(c_name + "=");
            }
            if (c_start == -1)
            {
                c_value = null;
            }
            else
            {
                c_start = c_value.indexOf("=", c_start) + 1;
                var c_end = c_value.indexOf(";", c_start);
                if (c_end == -1)
                {
                    c_end = c_value.length;
                }
                c_value = unescape(c_value.substring(c_start,c_end));
            }
            return c_value;
        }
    }
)