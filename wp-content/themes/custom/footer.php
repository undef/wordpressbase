<?php
    // uncomment to use the footer text
    // $footerText = of_get_option("footer","");if($footerText != "") echo "<div id='footer'>$footerText</div>";
?>

<?php
    //code to show the grid overlay
    if(isDebug()){
        echo "<a href='".get_template_directory_uri()."/debugger.php?url=".$_SERVER['PHP_SELF']."' class='debugButton'></a>";
    }
?>

<?php wp_footer(); ?>

</body>
</html>
