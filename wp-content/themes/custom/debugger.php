<?php include "../../../wp-load.php" ?>
<!DOCTYPE html>
<head>
    <!-- inspired by http://morganesque.github.io/resphrame/ -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>debugger</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/debugger.css">
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/debugger.js"></script>
<body>
<div class="header">
    <div class="close"><a href="<?php echo $_GET['url'];?>"><img src="img/debuggerClose.png" /></a></div>
    <div class="sizes"></div><div class="grid">|<a class="gridSelect" id="toggleGrid">GRID</a><a class="gridSelect" id="toggleOutlines">OUTLINES</a></div>
</div>
<div class="markers"></div>
<div class="cover"></div>
<div class="wrapper"><iframe id="iframe" src="<?php echo $_GET['url'];?>"></iframe></div>
</body>
</html>