<?php

add_action( 'init', 'createCustomPostTypes' );

function createCustomPostTypes() {
    //see http://codex.wordpress.org/Function_Reference/register_post_type for options
    register_post_type( 'custom',
        array(
            'labels' => array(
                'name' => __( 'Customs' ),
                'singular_name' => __( 'Custom' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
    flush_rewrite_rules();
}